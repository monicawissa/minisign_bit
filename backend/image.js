const { sprintf } = require('sprintf-js');
const { read } = require('jimp');
const { tmpdir } = require('os');
const { join } = require('path');
const extractFrame = require('ffmpeg-extract-frames');

exports.extract_frames = function (vid_path) {
	const out_path = join(tmpdir(), 'frame-%d.jpg');
	return extractFrame({
		input: vid_path,
		output: out_path,
		numFrames: 10,
	}).then(() => {
		const paths = [];
		for (let i = 0; i < 10; i++) {
			paths[i] = sprintf(out_path, i + 1);
		}
		return paths;
	});
};

exports.read_frame = function (frame_path) {
	console.log(frame_path);
	return read(frame_path)
		.then((img) => img.resize(64, 64))
		.then((img) => {
			const frame = [];
			for (let y = 0; y < img.bitmap.height; y++) {
				for (let x = 0; x < img.bitmap.width; x++) {
					const idx = img.getPixelIndex(x, y);
					if (frame[y] === undefined) frame[y] = [];
					if (frame[y][x] === undefined) frame[y][x] = [];

					frame[y][x][0] = img.bitmap.data[idx + 0];
					frame[y][x][1] = img.bitmap.data[idx + 1];
					frame[y][x][2] = img.bitmap.data[idx + 2];
				}
			}
			return frame;
		});
};
