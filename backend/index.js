/* eslint-disable promise/no-nesting */
const admin = require('firebase-admin');
const functions = require('firebase-functions');
const tf = require('@tensorflow/tfjs-node');
const os = require('os');
const path = require('path');
const { extract_frames, read_frame } = require('./image');

admin.initializeApp();

function onlyName(name) {
	const list = name.split('/');
	n = list[list.length - 1];
	return n.split('.')[0];
}
exports.readNewVid = functions
	.runWith({
		timeoutSeconds: 540,
		memory: '1GB',
	})
	.storage.object()
	.onFinalize(async (object) => {
		// note get firebase bucket
		const bucket = admin.storage().bucket(object.bucket);
		// note read video name
		const tempFilePath = path.join(
			os.tmpdir(),
			object.name.toString().split('/')[1]
		);
		if (object.name.split('/')[0] !== 'msgVideos') return;
		console.log({
			tempFilePath: tempFilePath,
		});
		console.log('downloading');
		//note donload the video
		await bucket
			.file(object.name)
			.download({
				destination: tempFilePath,
			})
			.then()
			.catch((err) => {
				console.log({
					type: 'video download err',
					err: err,
				});
				return;
			});
		console.log('reading');
		const names = [
			'INFORM',
			'ART',
			'SHELF',
			'COLLECT',
			'MACHINE',
			'CUTE',
			'EAT',
			'EXCUSE',
			'DRESS',
			'LOOK',
		];
		//note extract frames
		extract_frames(tempFilePath)
			//note read each frame
			.then((paths) => paths.map(read_frame))
			//note wait untill read each frame
			.then((frames) => Promise.all(frames))
			//note work on 4D array (time, width, height and colors)
			.then((frames) => {
				console.log('done converting to frames');
				// console.log(frames);
				//note load ML model
				return tf
					.loadLayersModel(
						'https://firebasestorage.googleapis.com/v0/b/last-minisign.appspot.com/o/model%2Fmodel.json?alt=media&token=eee39fe4-3625-4578-8aae-185a63b533ef'
					)
					.then((model) => {
						// note convert from 4D array to 4D tensors for the model
						console.log('loading the model is done');
						const frames_tensor = tf.tensor(frames).expandDims(0);
						console.log('predecting');
						// note pridict
						const prediction = model.predict(frames_tensor);
						// console.log({predect: prediction,});
						const class_no = prediction.argMax(1).dataSync()[0];
						const class_name = names[class_no];
						console.log({
							class_name: class_name,
						});
						// note constract file name
						const name = onlyName(tempFilePath);
						console.log('videosPath/msgVideos/' + name);
						// TODO REOMVE EXTENTION
						// note get message path by video name
						return admin
							.database()
							.ref('videosPath/msgVideos/')
							.once('value')
							.then((data) => {
								console.log('saveing class name');
								const path = data.toJSON()[name].toString();
								// note save the class name in database
								return admin
									.database()
									.ref(path.substring(36, path.length - 17))
									.update({
										translatedTo_text: class_name,
									})
									.then((_) => {
										console.log('done saveing class name');
										return Promise.resolve(0);
									})
									.catch((err) => {
										console.log(
											`failed to get message path ${err}`
										);
									});
							})
							.catch((_) => {
								console.log(
									`failed to save video translate ${err}`
								);
							});
					})
					.catch((err) => {
						console.error(`failed to load model ${err}`);
					});
			})
			.catch((err) => {
				console.log(err);
			});
		// code ends here -----------------------------
	});
exports.split = functions.https.onRequest(async (request, response) => {
	console.log('----------------------------------------------');
	response.send({ data: 'done' });
});
exports.helloWorld = functions.https.onRequest(async (request, response) => {
	console.log('----------------------------------------------');
	const name = request.query.name;
	const class_name = request.query.class;
	admin
		.database()
		.ref('videosPath/msgVideos/')
		.once('value')
		.then((data) => {
			const path = data.toJSON()[name].toString();
			// note save the class name in database
			return admin
				.database()
				.ref(path.substring(36, path.length - 17))
				.update({
					translatedTo_text: class_name,
				})
				.then((_) => {
					return Promise.resolve(0);
				})
				.catch((err) => {
					console.log(`failed to get message path ${err}`);
				});
		})
		.catch((_) => {
			console.log(`failed to save video translate ${err}`);
		});
	response.send('done');
});

exports.test = functions
	.runWith({
		timeoutSeconds: 540,
		memory: '1GB',
	})
	.https.onRequest(async (request, response) => {
		const bucket = admin.storage().bucket(request.query.bucket);
		const name = request.query.name;
		console.log({ name: request.query.name, bucket: request.query.bucket });

		const tempFilePath = path.join(
			os.tmpdir(),
			name.toString().split('/')[1]
		);

		if (name.split('/')[0] !== 'msgVideos') return;
		console.log({
			tempFilePath: tempFilePath,
		});
		console.log('downloading');
		//note donload the video
		await bucket
			.file(name)
			.download({
				destination: tempFilePath,
			})
			.then()
			.catch((err) => {
				console.log({
					type: 'video download err',
					err: err,
				});
				return;
			});
		console.log('reading');
		const names = [
			'INFORM',
			'ART',
			'SHELF',
			'COLLECT',
			'MACHINE',
			'CUTE',
			'EAT',
			'EXCUSE',
			'DRESS',
			'LOOK',
		];
		//note extract frames
		extract_frames(tempFilePath)
			//note read each frame
			.then((paths) => paths.map(read_frame))
			//note wait untill read each frame
			.then((frames) => Promise.all(frames))
			//note work on 4D array (time, width, height and colors)
			.then((frames) => {
				console.log('done converting to frames');
				// console.log(frames);
				//note load ML model
				return tf
					.loadLayersModel(
						'https://firebasestorage.googleapis.com/v0/b/last-minisign.appspot.com/o/model%2Fmodel.json?alt=media&token=eee39fe4-3625-4578-8aae-185a63b533ef'
					)
					.then((model) => {
						// note convert from 4D array to 4D tensors for the model
						console.log('loading the model is done');
						const frames_tensor = tf.tensor(frames).expandDims(0);
						console.log('predecting');
						// note pridict
						const prediction = model.predict(frames_tensor);
						// console.log({predect: prediction,});
						const class_no = prediction.argMax(1).dataSync()[0];
						const class_name = names[class_no];
						console.log({
							class_name: class_name,
						});
						// note constract file name
						const name = onlyName(tempFilePath);
						console.log('videosPath/msgVideos/' + name);
						// TODO REOMVE EXTENTION
						// note get message path by video name
						return admin
							.database()
							.ref('videosPath/msgVideos/')
							.once('value')
							.then((data) => {
								// console.log(data);
								const path = data.toJSON()[name].toString();
								console.log('saveing class name');
								// note save the class name in database
								return admin
									.database()
									.ref(path.substring(36, path.length - 17))
									.update({
										translatedTo_text: class_name,
									})
									.then((_) => {
										console.log('done save class name');
										return Promise.resolve(0);
									})
									.catch((err) => {
										console.log(
											`failed to get message path ${err}`
										);
									});
							})
							.catch((_) => {
								console.log(
									`failed to save video translate ${err}`
								);
							});
					})
					.catch((err) => {
						console.error(`failed to load model ${err}`);
					});
			})
			.catch((err) => {
				console.log(err);
			});
		// code ends here -----------------------------
		response.send('done');
	});
