package com.example.minisign.network;



public class Database {

    public static final String NODE_USERS = "Users";
    public static final String NODE_USER_CHATS = "users-chats";
    public static final String NODE_MESSAGES = "messages";
    public static final String NODE_VIDEOPATH = "videosPath";
}
