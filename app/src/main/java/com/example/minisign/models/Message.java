package com.example.minisign.models;

import com.google.firebase.database.IgnoreExtraProperties;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


@IgnoreExtraProperties
public class Message {
    @SerializedName("id")
    @Expose
    private String id="";
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("data")
    @Expose
    private String data;
    // if receiver not download video ="" ,if downloaded=video Recieved path
    @SerializedName("status")
    @Expose
    private String status="";

    public String getMyVideo_path() {
        return myVideo_path;
    }

    public void setMyVideo_path(String myVideo_path) {
        this.myVideo_path = myVideo_path;
    }

    @SerializedName("myVideo_path")
    @Expose
    private String myVideo_path="";
    @SerializedName("senderUid")
    @Expose
    private String senderUid;
    @SerializedName("receiverUid")
    @Expose
    private String receiverUid;
    // null or the text
    @SerializedName("TranslatedTo_text")
    @Expose
    private String TranslatedTo_text="";
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTranslatedTo_text() {
        return TranslatedTo_text;
    }

    public void setTranslatedTo_text(String translatedTo_text) {
        TranslatedTo_text = translatedTo_text;
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSenderUid() {
        return senderUid;
    }

    public void setSenderUid(String senderUid) {
        this.senderUid = senderUid;
    }

    public String getReceiverUid() {
        return receiverUid;
    }

    public void setReceiverUid(String receiverUid) {
        this.receiverUid = receiverUid;
    }

}

