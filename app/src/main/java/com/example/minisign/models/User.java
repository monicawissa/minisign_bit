package com.example.minisign.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class User implements Parcelable {

    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("status")
    @Expose
    private String status="";
    @SerializedName("profile_pic_url")
    @Expose
    private String profilePicUrl = "";
    @SerializedName("last_seen")
    @Expose
    private String lastSeen = "";
    @SerializedName("is_typing")
    @Expose
    private Boolean isTyping = false;
    @SerializedName("uid")
    @Expose
    private String uid = "";

    public String getProfilePicUrl() {
        return profilePicUrl;
    }

    public void setProfilePicUrl(String profilePicUrl) {
        this.profilePicUrl = profilePicUrl;
    }

    public String getLastSeen() {
        return lastSeen;
    }

    public void setLastSeen(String lastSeen) {
        this.lastSeen = lastSeen;
    }

    public Boolean getTyping() {
        return isTyping;
    }

    public void setTyping(Boolean typing) {
        isTyping = typing;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }


    protected User(Parcel in) {
        this.firstName = in.readString();
        this.lastName =in.readString();
        this.email =in.readString();
        this.phone = in.readString();
        this.password = in.readString();
        this.status = in.readString();
        this.profilePicUrl =in.readString();
        this.lastSeen = in.readString();
        this.isTyping = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.uid = in.readString();
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public User() {
    }


    public User(String firstName, String lastName, String email, String phone, String password) {
        super();
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.status="";
        this.profilePicUrl="";
    }
    public User(String firstName, String lastName, String email, String phone, String password,String status) {
        super();
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.status=status;this.profilePicUrl="";
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.firstName);
        dest.writeString(this.lastName);
        dest.writeString(this.email);
        dest.writeString(this.password);
        dest.writeString(this.phone);
        dest.writeString(this.status);
        dest.writeString(this.profilePicUrl);
        dest.writeString(this.lastSeen);
        dest.writeValue(this.isTyping);
        dest.writeString(this.uid);

    }
    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}