package com.example.minisign.ui.main;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.minisign.R;
import com.example.minisign.network.Database;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;


public class ProfileFragment extends Fragment {
    private static final int Galleries = 1;
    private StorageReference userProfileImageRef;
    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;
    private ProgressDialog dialog;
    private DatabaseReference myRef ;
    private String currentUserId;
    TextInputLayout name_textInputLayout;
    TextInputLayout status_textInputLayout;
    TextInputLayout phone_textInputLayout;
    CircleImageView photo_CircleImageView;
    Button btnupdate_data ;
    private Fragment fragg = this;
    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_profile, container, false);
        name_textInputLayout=v.findViewById(R.id.name_editText);
        status_textInputLayout=v.findViewById(R.id.status_editText);
        phone_textInputLayout=v.findViewById(R.id.phone_editText);
        photo_CircleImageView=v.findViewById(R.id.profile_image);
        btnupdate_data=v.findViewById(R.id.update_data);
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        myRef=FirebaseDatabase.getInstance().getReference();
        dialog=new ProgressDialog(getContext());
        setuserdata();
        userProfileImageRef = FirebaseStorage.getInstance().getReference().child("Profile Images");
        photo_CircleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent galleryIntent=new Intent();
                galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
                galleryIntent.setType("image/*");
                Toast.makeText(getContext(),"go to gallery",Toast.LENGTH_SHORT).show();
                /** Pass your fragment reference **/
                fragg.startActivityForResult(galleryIntent, Galleries);
            }
        });
        btnupdate_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                update_data();
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode== Galleries && resultCode == RESULT_OK && data != null)
        {
            Uri imageUri=data.getData();
            Intent intent = CropImage.activity(imageUri)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setAspectRatio(16,9)
                    .getIntent(getContext());

            fragg.startActivityForResult(intent, CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE);

        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE)
        {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if(resultCode == RESULT_OK)
            {
                dialog.setTitle("Set profile photo");
                dialog.setMessage("please wait, your image is uploading...");
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
                final Uri resultUri = result.getUri();
                Picasso.get().load(resultUri).placeholder(R.drawable.pic).into(photo_CircleImageView);
                final StorageReference filepath = userProfileImageRef.child(currentUserId+".jpg");
                filepath.putFile(resultUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        Toast.makeText(getContext(), "Photo uploaded successfully..", Toast.LENGTH_LONG).show();
                        //String downloaedUrl = taskSnapshot.getUploadSessionUri().toString();
                        filepath.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(final Uri uri) {

                                myRef.child(Database.NODE_USERS).child(currentUserId).child("profilePicUrl").setValue(uri.toString())
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                Toast.makeText(getContext(), "Photo save in DB successfully..", Toast.LENGTH_LONG).show();
                                                dialog.dismiss();

                                            }
                                        }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {

                                        Toast.makeText(getContext(), "Error " + e.toString(), Toast.LENGTH_LONG).show();
                                        dialog.dismiss();

                                    }
                                });
                            }
                        });
                    }
                });
                setuserdata();

            }
            else{
                Toast.makeText(getContext(),"resultCode != RESULT_OK",Toast.LENGTH_LONG).show();

            }
        }
    }

    private void setuserdata() {
        currentUserId= Objects.requireNonNull(mAuth.getCurrentUser()).getUid();
        myRef.child("Users").child(currentUserId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if((dataSnapshot.child("firstName").exists())){
                    String name=dataSnapshot.child("firstName").getValue().toString();
                    Objects.requireNonNull(name_textInputLayout.getEditText()).setText(name);
                }
                if((dataSnapshot.child("status").exists())){
                    String name=dataSnapshot.child("status").getValue().toString();
                    Objects.requireNonNull(status_textInputLayout.getEditText()).setText(name);
                }
                if((dataSnapshot.child("phone").exists())){
                    String name=dataSnapshot.child("phone").getValue().toString();
                    Objects.requireNonNull(phone_textInputLayout.getEditText()).setText(name);
                }
                if ((dataSnapshot.child("profilePicUrl").exists()&&!dataSnapshot.child("profilePicUrl").getValue().equals("")))
                {
                    String retrieveProfileImage = dataSnapshot.child("profilePicUrl").getValue().toString();

                    //Toast.makeText(SettingsActivity.this,photo_CircleImageView.toString(),Toast.LENGTH_LONG).show();
                    Picasso.get().load(retrieveProfileImage).placeholder(R.drawable.pic).into(photo_CircleImageView);
                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void update_data() {
        boolean valid = true;
        String name = name_textInputLayout.getEditText().getText().toString().trim();
        String status = status_textInputLayout.getEditText().getText().toString().trim();
        String phone = phone_textInputLayout.getEditText().getText().toString().trim();
        if (TextUtils.isEmpty(name)) {
            name_textInputLayout.setError(getResources().getString(R.string.required));
            valid = false;
        } else {
            name_textInputLayout.setError(null);
        }
        if (TextUtils.isEmpty(status)) {
            status_textInputLayout.setError(getResources().getString(R.string.required));
            valid = false;
        } else {
            status_textInputLayout.setError(null);
        }
        if (TextUtils.isEmpty(phone)) {
            phone_textInputLayout.setError(getResources().getString(R.string.required));
            valid = false;
        } else {
            phone_textInputLayout.setError(null);
        }
        if(!valid)return;
        final Map<String,Object> profile_map=new HashMap<>();
        profile_map.put("firstName", name);
        profile_map.put("status", status);
        profile_map.put("phone", phone);
        myRef.child("Users").child(currentUserId).updateChildren(profile_map).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()) {
                    Toast.makeText(getContext(), "profile updated", Toast.LENGTH_SHORT).show();
                }
                else Toast.makeText(getContext(), task.getException().toString(), Toast.LENGTH_SHORT).show();

            }
        });
    }
}
