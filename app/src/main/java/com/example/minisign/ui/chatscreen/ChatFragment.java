package com.example.minisign.ui.chatscreen;

import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.minisign.R;
import com.example.minisign.models.ChatMessage;
import com.example.minisign.models.Message;
import com.example.minisign.models.User;
import com.example.minisign.models.UserChat;
import com.example.minisign.network.Database;
import com.example.minisign.network.Util;
import com.firebase.ui.database.ChangeEventListener;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static com.example.minisign.ui.chatscreen.ChatActivity.EXTRAS_USER;

public class ChatFragment extends Fragment {

    public static ChatFragment newInstance(Bundle bundle) {
        ChatFragment fragment = new ChatFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    private FirebaseDatabase mDatabase;
    private StorageReference msgvideosRef;
    private FirebaseAuth mAuth;
    private User mReceivingUser;
    private ImageView takevideo;
    //private ProgressDialog dialog;
    private static int Video_Request=101;
    private ProgressBar pgsBar;
    private Uri videoUri;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance();
        Bundle bundle = getArguments();
        mReceivingUser = bundle.getParcelable(EXTRAS_USER);
        msgvideosRef = FirebaseStorage.getInstance().getReference().child("msgVideos");
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable final Intent data) {
        if(requestCode==Video_Request&&resultCode==RESULT_OK){
            videoUri=data.getData();
            pgsBar.setVisibility(View.VISIBLE);
            final StorageReference filepath = msgvideosRef.child(data.getData().getLastPathSegment());
            Toast.makeText(getContext(),data.getData().getLastPathSegment(),Toast.LENGTH_SHORT).show();
            filepath.putFile(videoUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    filepath.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(final Uri uri) {
                            //
                            downloadfile(getContext(),data.getData().getLastPathSegment(),".mp4","msgVideos",uri.toString());
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getContext(), ""+e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    });
                    pgsBar.setVisibility(View.INVISIBLE);
                }
            });
        }
    }



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_chat, container, false);
        pgsBar = (ProgressBar)view.findViewById(R.id.progress_bar);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
        loadChats();
    }

    private void initViews() {
        initMessageBar();
        initRecyclerView();
    }


    private FloatingActionButton mFabButton;
    private EditText mEditText;
    private void initMessageBar() {
        //dialog=new ProgressDialog(getContext());
        takevideo=getView().findViewById(R.id.video_taking);
        mEditText = (EditText) getView().findViewById(R.id.editText);
        mEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
            }
        });

        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (s.length() == 0) {
                    showSendButton();
                    //Toast.makeText(getContext(), "showSendButton", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    showAudioButton();
                }
            }
        });//test2
        takevideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent videointent= new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                if(videointent.resolveActivity(getActivity().getPackageManager())!=null){
//                    Uri fileUri = getOutputMediaFileUri(2);
//                    videointent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
//                    videointent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
                    startActivityForResult(videointent,Video_Request);
                }
            }
        });

        mFabButton = (FloatingActionButton) getView().findViewById(R.id.floatingButton);
        mFabButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String tag = (String) mFabButton.getTag();
                Log.d("fab tag", tag);
                if (tag.equalsIgnoreCase(SEND_IMAGE)) {
                    onSendButtonClicked();
                }
                // audio click

            }
        });
    }

    private RecyclerView mRecyclerView;

    private void initRecyclerView() {
        mRecyclerView = (RecyclerView) getView().findViewById(R.id.chatRecyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setStackFromEnd(true);
        mRecyclerView.setLayoutManager(linearLayoutManager);
    }


    private static final String SEND_IMAGE = "send_image";

    private void showSendButton() {
        mFabButton.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.input_send));
        mFabButton.setTag(SEND_IMAGE);
    }

    private static final String MIC_IMAGE = "mic_image";

    private void showAudioButton() {
        mFabButton.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.input_mic_white));
        mFabButton.setTag(MIC_IMAGE);
    }

    private void onSendButtonClicked() {
        String message = mEditText.getText().toString();
        mEditText.setText("");
        Log.d("send msg", message);
        writeTextMessage(message,"text","","");
    }

    private void writeTextMessage(String data,String type,String receivedVideo_path,String myVideo_path) {
        Message message = new Message();

        message.setSenderUid(mAuth.getCurrentUser().getUid());
        message.setReceiverUid(mReceivingUser.getUid());
        message.setType(type);
        message.setData(data);
        message.setStatus(receivedVideo_path);
        message.setMyVideo_path(myVideo_path);
        String messagesNode = getMessagesNode();
        String id = mDatabase.getReference().child(Database.NODE_MESSAGES).child(messagesNode).push().getKey();
        message.setId(id);
        mDatabase.getReference().child(Database.NODE_MESSAGES).child(messagesNode).child(id).setValue(message);

        //update userchats
        FirebaseUser sendingUser = mAuth.getCurrentUser();

        UserChat userChat = new UserChat();
        userChat.setUid(sendingUser.getUid());
        if(message.getType().equals("video")){
            userChat.setLastMessage("video");
            // set video path
            String entry = mDatabase.getReference().child(Database.NODE_MESSAGES).child(messagesNode).child(id).child("TranslatedTo_text").toString();
            mDatabase.getReference().child(Database.NODE_VIDEOPATH).child(Uri.parse(data).getLastPathSegment()).setValue(entry);
        }
        else
            userChat.setLastMessage(data);
        // add 'last time' and count 'number of msg sent'

//        String userChatKey = mDatabase.getReference().child(Database.NODE_USER_CHATS).child(mReceivingUser.getUid()).push().getKey();

        // add User chat in sending User
        mDatabase.getReference().child(Database.NODE_USER_CHATS).child(mReceivingUser.getUid()).child(sendingUser.getUid()).setValue(userChat);


        userChat.setUid(mReceivingUser.getUid());
        // add User chat in receiving User
        mDatabase.getReference().child(Database.NODE_USER_CHATS).child(sendingUser.getUid()).child(mReceivingUser.getUid()).setValue(userChat);

    }


    private String getMessagesNode() {
        String messageNode = null;
        if (mAuth.getCurrentUser() != null) {
            FirebaseUser firebaseUser = mAuth.getCurrentUser();
            String sendingUID = firebaseUser.getUid();
            String receivingUID = mReceivingUser.getUid();
            messageNode = Util.getMessageNode(sendingUID, receivingUID);
        }
        return messageNode;
    }

    //private ChatAdapter mChatAdapter;

//    private void showChats() {
//        try {
//            List<ChatMessage> chatMessages = getChatMessages();
//            mChatAdapter = new ChatAdapter(getActivity(), chatMessages);
//            mRecyclerView.setAdapter(mChatAdapter);
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//    }
//
//    private List<ChatMessage> getChatMessages() throws IOException, JSONException {
//        List<ChatMessage> chatMessages = null;
//
//        JSONObject jsonObject;
//        String json;
//
//        InputStream is = getActivity().getAssets().open("chatmessages.json");
//
//        int size = is.available();
//        byte[] buffer = new byte[size];
//        is.read(buffer);
//        is.close();
//        json = new String(buffer, "UTF-8");
//
//        jsonObject = new JSONObject(json);
//        JSONArray jsonArray = (JSONArray) jsonObject.get("1");
//
//        Type listType = new TypeToken<List<ChatMessage>>() {
//        }.getType();
//
//        chatMessages = new Gson().fromJson(jsonArray.toString(), listType);
//
//        return chatMessages;
//
//    }

    private FirebaseRecyclerAdapter<Message, RecyclerView.ViewHolder> mAdapter;
    private void downloadfile(Context context, String fileName, String fileEx, String destinationDirectory, final String url) {
        final DownloadManager downloadManager =(DownloadManager)context.getSystemService(context.DOWNLOAD_SERVICE);
        final Uri uri=Uri.parse(url);
        DownloadManager.Request request=new DownloadManager.Request(uri);
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setAllowedOverRoaming(false);
        request.setVisibleInDownloadsUi(true);
        request.setDestinationInExternalFilesDir(context,destinationDirectory,fileName+fileEx);
        assert downloadManager != null;
        // the problem was that we need to wait for the enqueu"download the video " to finish
        final long id=downloadManager.enqueue(request);
        BroadcastReceiver onComplete  = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                    long downloadId = intent.getLongExtra(
                            DownloadManager.EXTRA_DOWNLOAD_ID, 0);
                    Uri myUri= downloadManager.getUriForDownloadedFile(id);
                    writeTextMessage(url,"video","",myUri.toString());

                }
            }
        };

        context.registerReceiver(onComplete , new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
        return ;
    }
    private void downloadfile2(Context context, String fileName, String fileEx, String destinationDirectory, final String url, final Message message, final VideoView chatvideoV) {
        final DownloadManager downloadManager =(DownloadManager)context.getSystemService(context.DOWNLOAD_SERVICE);
        final Uri uri=Uri.parse(url);
        DownloadManager.Request request=new DownloadManager.Request(uri);
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setAllowedOverRoaming(false);
        request.setVisibleInDownloadsUi(true);
        request.setDestinationInExternalFilesDir(context,destinationDirectory,fileName+fileEx);
        assert downloadManager != null;
        // the problem was that we need to wait for the enqueu"download the video " to finish
        final long id=downloadManager.enqueue(request);
        BroadcastReceiver onComplete2  = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                    long downloadId = intent.getLongExtra(
                            DownloadManager.EXTRA_DOWNLOAD_ID, 0);
                    Uri myUri= downloadManager.getUriForDownloadedFile(id);
                    message.setStatus(myUri.toString());
                    mDatabase.getReference().child(Database.NODE_MESSAGES)
                            .child(getMessagesNode()).child(message.getId()).setValue(message);
                    chatvideoV.setVideoURI(myUri);
                    MediaController mediaController=new MediaController(getContext());
                    chatvideoV.setMediaController(mediaController);
                    mediaController.setAnchorView(chatvideoV);
                    pgsBar.setVisibility(View.INVISIBLE);
                }
            }
        };

        context.registerReceiver(onComplete2 , new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
        return ;
    }

//    /** Create a file Uri for saving an image or video */
//    private static Uri getOutputMediaFileUri(int type){
//
//        return Uri.fromFile(getOutputMediaFile(type));
//    }
//
//    /** Create a File for saving an image or video */
//    private static File getOutputMediaFile(int type){
//
//        // Check that the SDCard is mounted
//        String root = Environment.getExternalStorageDirectory().toString();
//        File mediaStorageDir = new File(root + "/msgVideos");
//
//        // Create the storage directory(MyCameraVideo) if it does not exist
//        if (! mediaStorageDir.exists()){
//
//            if (! mediaStorageDir.mkdirs()){
//                Log.d("MySignVideoS", "Failed to create directory MyCameraVideo.");
//                return null;
//            }
//        }
//        // Create a media file name
//
//        // For unique file name appending current timeStamp with file name
//        java.util.Date date= new java.util.Date();
//        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
//                .format(date.getTime());
//
//        File mediaFile;
//
//        if(type == 2) {
//
//            // For unique video file name appending current timeStamp with file name
//            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
//                    "VID_"+ timeStamp + ".mp4");
//
//        } else {
//            return null;
//        }
//
//        return mediaFile;
//    }
    private void loadChats() {

        String messageNode = getMessagesNode();
        Query messageQuery = mDatabase.getReference().child(Database.NODE_MESSAGES).child(messageNode);

        /*
       messageQuery.addValueEventListener(new ValueEventListener() {
           @Override
           public void onDataChange(DataSnapshot dataSnapshot) {

               try
               {
                   Message message = dataSnapshot.getValue(Message.class);
                   message.getData();
               }catch (Exception e)
               {
                   e.printStackTrace();

               }

           }

           @Override
           public void onCancelled(DatabaseError databaseError) {

           }
       });
       */


        mAdapter = new FirebaseRecyclerAdapter<Message, RecyclerView.ViewHolder>(Message.class, R.layout.item_messsage_outgoing,
                RecyclerView.ViewHolder.class, messageQuery) {

            private final int TYPE_INCOMING = 1;
            private final int TYPE_OUTGOING = 2;

            @Override
            protected void populateViewHolder(final RecyclerView.ViewHolder viewHolder, final Message message, final int position) {

                if (messageFromCurrentUser(message)) {
                    populateOutgoingViewHolder((OutgoingViewHolder) viewHolder, message);
                } else {
                    populateIncomingViewHolder((IncomingViewHolder) viewHolder, message);
                }
            }

            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view;
                switch (viewType) {
                    case TYPE_INCOMING:

                        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_messsage_incoming, parent, false);
                        return new IncomingViewHolder(view);

                    case TYPE_OUTGOING:
                        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_messsage_outgoing, parent, false);
                        return new OutgoingViewHolder(view);
                }
                return super.onCreateViewHolder(parent, viewType);
            }

            private void populateIncomingViewHolder(IncomingViewHolder viewHolder, Message message) {
                // Bind Post to ViewHolder, setting OnClickListener for the star button
                viewHolder.bindToMessage(message, new View.OnClickListener() {
                    @Override
                    public void onClick(View starView) {

                    }
                });

            }

            private void populateOutgoingViewHolder(OutgoingViewHolder viewHolder, Message message) {

                // Bind Post to ViewHolder, setting OnClickListener for the star button
                viewHolder.bindToMessage(message, new View.OnClickListener() {
                    @Override
                    public void onClick(View starView) {
                    }
                });

            }

            @Override
            public int getItemViewType(int position) {
                super.getItemViewType(position);
                Message message = getItem(position);

                if (messageFromCurrentUser(message)) {
                    return TYPE_OUTGOING;
                }
                return TYPE_INCOMING;
            }
            //check message From Current User
            private boolean messageFromCurrentUser(Message message) {
                String currentUid = mAuth.getCurrentUser().getUid();
                if (currentUid.equalsIgnoreCase(message.getSenderUid())) {
                    return true;
                }
                return false;
            }

            class IncomingViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

                TextView chatTextV, timeTV;
                VideoView chatvideoV;
//                       messageStatusIV;

                public IncomingViewHolder(View v) {
                    super(v);
                    chatTextV = (TextView) v.findViewById(R.id.chatTV);
                    timeTV = (TextView) v.findViewById(R.id.timeTV);
                    chatvideoV = (VideoView) v.findViewById(R.id.chatVideoView2);
                }

                @Override
                public void onClick(View v) {

                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                    }

                }

                public void bindToMessage(final Message message, View.OnClickListener starClickListener) {

                    if (message.getType().equals("text") ){
                        chatTextV.setVisibility(View.VISIBLE);
                        timeTV.setVisibility(View.VISIBLE);
                        chatvideoV.setVisibility(View.GONE);
                        chatTextV.setText(message.getData());}
                    else if (message.getType().equals("video")){
                        chatTextV.setVisibility(View.GONE);
                        timeTV.setVisibility(View.GONE);
                        chatvideoV.setVisibility(View.VISIBLE);
                        String currentUid = mAuth.getCurrentUser().getUid();
                        //if receiver not downloaded video before =""
                        if((message.getStatus().isEmpty()||message.getStatus()==null)){
                            chatvideoV.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    pgsBar.setVisibility(View.VISIBLE);
                                    Uri uri=Uri.parse(message.getData());
                                    String s=uri.getLastPathSegment();
                                    String video_name=s.substring(s.lastIndexOf('/') + 1);
                                    downloadfile2(getContext(),video_name,".mp4","msgVideos",message.getData(),message,chatvideoV);

                                }
                            });

                        }
                        else{
                        //Toast.makeText(getContext(),"already downloaded",Toast.LENGTH_LONG).show();
                        chatvideoV.setVideoURI(Uri.parse(message.getStatus()));
                        MediaController mediaController=new MediaController(getContext());
                        chatvideoV.setMediaController(mediaController);
                        mediaController.setAnchorView(chatvideoV);
                        chatvideoV.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                            @Override
                            public boolean onError(MediaPlayer mp, int what, int extra) {
                                pgsBar.setVisibility(View.VISIBLE);
                                Toast.makeText(getContext(),"Video Error Incomming",Toast.LENGTH_LONG).show();
                                Uri uri=Uri.parse(message.getData());
                                String s=uri.getLastPathSegment();
                                String video_name=s.substring(s.lastIndexOf('/') + 1);
                                downloadfile2(getContext(),video_name,".mp4","msgVideos",message.getData(),message,chatvideoV);
                                return false;
                            }
                        });
                        chatvideoV.setOnLongClickListener(new View.OnLongClickListener() {
                            @SuppressLint("SetTextI18n")
                            @Override
                            public boolean onLongClick(View v) {
                                if(!message.getTranslatedTo_text().isEmpty()&&message.getTranslatedTo_text()!=null){
                                    chatTextV.setText( getString(R.string.vid_trans_title) +message.getTranslatedTo_text());
                                    chatTextV.setVisibility(View.VISIBLE);
                                    timeTV.setVisibility(View.VISIBLE);
                                    chatvideoV.setVisibility(View.GONE);

                                }
                                return false;
                            }
                        });
                        chatTextV.setOnLongClickListener(new View.OnLongClickListener() {
                            @Override
                            public boolean onLongClick(View v) {
                                chatTextV.setVisibility(View.GONE);
                                timeTV.setVisibility(View.GONE);
                                chatvideoV.setVisibility(View.VISIBLE);
                                return false;
                            }
                        });
                        }
                    }

                }

            }

            class OutgoingViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

                TextView chatTextV, timeTV;
                VideoView  chatvideoV;
                public OutgoingViewHolder(View v) {
                    super(v);
                    chatTextV = (TextView) v.findViewById(R.id.chatTV);
                    timeTV = (TextView) v.findViewById(R.id.timeTV);
                    chatvideoV = (VideoView) v.findViewById(R.id.chatVideoView);
                }

                @Override
                public void onClick(View v) {

                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                    }
                }

                public void bindToMessage(final Message message, View.OnClickListener starClickListener) {
                    if (message.getType().equals("text") ){
                        chatTextV.setVisibility(View.VISIBLE);
                        timeTV.setVisibility(View.VISIBLE);
                        chatvideoV.setVisibility(View.GONE);
                        chatTextV.setText(message.getData());}
                    else if (message.getType().equals("video")){
                        chatTextV.setVisibility(View.GONE);
                        timeTV.setVisibility(View.GONE);
                        chatvideoV.setVisibility(View.VISIBLE);
                        chatvideoV.setVideoURI(Uri.parse(message.getMyVideo_path()));
                        MediaController mediaController=new MediaController(getContext());
                        chatvideoV.setMediaController(mediaController);
                        mediaController.setAnchorView(chatvideoV);
                        chatvideoV.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                            @Override
                            public boolean onError(MediaPlayer mp, int what, int extra) {
                                Toast.makeText(getContext(), "Video Error outgoing", Toast.LENGTH_SHORT).show();
                                return false;
                                //test1
                            }
                        });
                        chatvideoV.setOnLongClickListener(new View.OnLongClickListener() {
                            @Override
                            public boolean onLongClick(View v) {
                                if(!message.getTranslatedTo_text().isEmpty()&&message.getTranslatedTo_text()!=null){
                                    chatTextV.setText(getString(R.string.vid_trans_title) +message.getTranslatedTo_text());
                                    chatTextV.setVisibility(View.VISIBLE);
                                    timeTV.setVisibility(View.VISIBLE);
                                    chatvideoV.setVisibility(View.GONE);

                                }
                                return false;
                            }
                        });
                        chatTextV.setOnLongClickListener(new View.OnLongClickListener() {
                            @Override
                            public boolean onLongClick(View v) {
                                chatTextV.setVisibility(View.GONE);
                                timeTV.setVisibility(View.GONE);
                                chatvideoV.setVisibility(View.VISIBLE);
                                return false;
                            }
                        });
                    }
                }
            }

            @Override
            protected void onChildChanged(ChangeEventListener.EventType type, int index, int oldIndex) {
                super.onChildChanged(type, index, oldIndex);

                int lastPostion;
                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
                lastPostion = linearLayoutManager.findLastVisibleItemPosition();
                if (lastPostion != -1 && type == ChangeEventListener.EventType.ADDED) {

                    if (index > lastPostion) {
                        onNewMessageReceived();
                    }
                }
            }

            private void onNewMessageReceived() {
                int position = mAdapter.getItemCount() - 1;
                mAdapter.notifyDataSetChanged();
                mRecyclerView.smoothScrollToPosition(mAdapter.getItemCount() - 1);
            }
        };

        mRecyclerView.setAdapter(mAdapter);

    }


}
