package com.example.minisign.ui.main.userListScreen;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.minisign.R;
import com.example.minisign.models.User;


public class UserViewHolder extends RecyclerView.ViewHolder {

    public ImageView userIV;
    public TextView usernameTV;
    public TextView statusTV;

    public UserViewHolder(View itemView) {
        super(itemView);

        userIV = (ImageView) itemView.findViewById(R.id.userIV);
        usernameTV = (TextView) itemView.findViewById(R.id.userNameTV);
        statusTV = (TextView) itemView.findViewById(R.id.statusTV);
    }

    public void bindToUser(User User, View.OnClickListener starClickListener) {

        if(!TextUtils.isEmpty(User.getProfilePicUrl()))
        {
            Glide.with(userIV.getContext()).load(User.getProfilePicUrl()).into(userIV);
        }
        usernameTV.setText(User.getFirstName());
        statusTV.setText(User.getStatus());
    }
}
