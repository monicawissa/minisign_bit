package com.example.minisign.ui.chatscreen;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentTransaction;

import com.bumptech.glide.Glide;
import com.example.minisign.R;
import com.example.minisign.models.Chat;
import com.example.minisign.models.User;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by vihaan on 21/05/17.
 */

public class ChatActivity extends AppCompatActivity {

    public static final String EXTRAS_USER= "User";
    public static final String tag = ChatActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        init();
    }

    private void init() {
        setArguments();
        initView();
    }

    private void initView() {
        initToolbar();
        showFragment();
    }

    private void showFragment()
    {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frameLayout, ChatFragment.newInstance(getIntent().getExtras()));
        ft.commit();
    }

    private Chat mChat;
    private User mUser;

    private void setArguments() {
        Bundle bundle = getIntent().getExtras();
        mUser = bundle.getParcelable(EXTRAS_USER);
        if(mUser != null)
        {
            setTitle(mUser.getFirstName());
        }
    }


    private void initToolbar()
    {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView nameTV = (TextView) findViewById(R.id.nameTV);
        nameTV.setText(mUser.getFirstName());

        CircleImageView userIV = (CircleImageView) findViewById(R.id.userIV);

        if(!TextUtils.isEmpty(mUser.getProfilePicUrl()))
        {
            Glide.with(this).load(mUser.getProfilePicUrl()).into(userIV);
        }

        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("Toolbar","Clicked");
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_chat, menu);
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(tag, " onStart()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(tag, " onResume()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(tag, " onPause()");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(tag, " onStop()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(tag, " onDestroy()");
    }


}
