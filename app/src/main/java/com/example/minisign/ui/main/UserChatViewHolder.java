package com.example.minisign.ui.main;

import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.minisign.R;
import com.example.minisign.models.User;
import com.example.minisign.models.UserChat;
import com.example.minisign.network.Database;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import de.hdodenhof.circleimageview.CircleImageView;



public class UserChatViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    RelativeLayout chatRL;
    CircleImageView userIV;
    TextView nameTV;
    TextView messageTV;
    TextView lastMessageTimeTV;
    TextView unreadMessageCountTV;

    public UserChatViewHolder(View itemView) {
        super(itemView);

        chatRL = (RelativeLayout) itemView.findViewById(R.id.chatRL);
        userIV = (CircleImageView) itemView.findViewById(R.id.userIV);
        nameTV = (TextView) itemView.findViewById(R.id.nameTV);
        messageTV = (TextView) itemView.findViewById(R.id.messageTV);
        lastMessageTimeTV = (TextView) itemView.findViewById(R.id.lastMessageTimeTV);
        unreadMessageCountTV = (TextView) itemView.findViewById(R.id.unreadMessageCountTV);
//        chatRL.setOnClickListener(this);
    }

    private ValueEventListener mValueEventListener;
    private DatabaseReference mUserNode;
    public void bindToUserChat(UserChat userChat, View.OnClickListener starClickListener) {

        String uid = userChat.getUid();

        mUserNode = FirebaseDatabase.getInstance().getReference().child(Database.NODE_USERS).child(uid);

        mValueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                User User = dataSnapshot.getValue(User.class);
                if (!TextUtils.isEmpty(User.getProfilePicUrl())) {
                    Glide.with(userIV.getContext()).load(User.getProfilePicUrl()).into(userIV);
                }
                nameTV.setText(User.getFirstName());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        mUserNode.addValueEventListener(mValueEventListener);

        messageTV.setText(userChat.getLastMessage());
        lastMessageTimeTV.setText(userChat.getLastMessageTime());

        if (TextUtils.isEmpty(userChat.getUnreadMessageCount())) {
            unreadMessageCountTV.setVisibility(View.GONE);
        } else {
            unreadMessageCountTV.setVisibility(View.VISIBLE);
            unreadMessageCountTV.setText(userChat.getUnreadMessageCount());
        }
    }

    @Override
    public void onClick(View v) {

    }

    public void onViewDetachedFromWindow()
    {
        if(mValueEventListener != null)
        {
            mUserNode.removeEventListener(mValueEventListener);
        }
    }
}
