package com.example.minisign.ui.main;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.example.minisign.R;
import com.example.minisign.ui.auth.loginActivity;
import com.example.minisign.ui.main.userListScreen.UserListFragment;
import com.google.firebase.auth.FirebaseAuth;
import butterknife.BindView;
import butterknife.ButterKnife;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.luseen.spacenavigation.SpaceItem;
import com.luseen.spacenavigation.SpaceNavigationView;
import com.luseen.spacenavigation.SpaceOnClickListener;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.meow_nav)
    SpaceNavigationView spaceNavigationView;
    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;
    private FirebaseDatabase database ;
    DatabaseReference myRef ;
    private final static int ID_HOME=0;
    private final static int ID_PROFILE=1;
    private final static int ID_Group=2;
    private final static int ID_LOGOUT=3;
    Fragment select_fragment=null;
    private int change=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        spaceNavigationView.initWithSaveInstanceState(savedInstanceState);
        //setSupportActionBar(toolbar);
        //getSupportActionBar().setTitle("");

        addingtabs();
        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
    }

    private void addingtabs() {
        spaceNavigationView.addSpaceItem(
                new SpaceItem(getString(R.string.home), R.drawable.ic_home));
        spaceNavigationView.addSpaceItem(
                new SpaceItem(getString(R.string.profile),R.drawable.ic_profile));
        spaceNavigationView.addSpaceItem(
                new SpaceItem(getString(R.string.group),R.drawable.ic_addgroup));
        spaceNavigationView.addSpaceItem(
                new SpaceItem(getString(R.string.logout), R.drawable.appstore));
        spaceNavigationView.showIconOnly();
        handlemenuClick();
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        spaceNavigationView.onSaveInstanceState(outState);
    }
    private void handlemenuClick() {
        getSupportFragmentManager().beginTransaction().replace(R.id.main_frame, new chatsFragment()).commit();

        spaceNavigationView.setSpaceOnClickListener(new SpaceOnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onCentreButtonClick() {
                Toast.makeText(MainActivity.this,"onCentreButtonClick", Toast.LENGTH_SHORT).show();
                change=1;
                select_fragment=new UserListFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.main_frame,select_fragment).commit();
            }

            @SuppressLint("ResourceAsColor")
            @Override
            public void onItemClick(int itemIndex, String itemName) {
                Toast.makeText(MainActivity.this, itemIndex + " " + itemName, Toast.LENGTH_SHORT).show();
                change=0;
                switch (itemIndex){
                    case ID_HOME:
                        select_fragment=new chatsFragment();
                        break;
                    case ID_Group:
                        select_fragment=new chatsFragment();
                        Toast.makeText(MainActivity.this,  "Soon will make" + itemName, Toast.LENGTH_SHORT).show();
                        break;
                    case ID_PROFILE:
                        select_fragment=new ProfileFragment();
                        break;
                    case ID_LOGOUT:
                        user_logout();
                    default:
                        select_fragment=new chatsFragment();
                        break;

                }
                assert select_fragment != null;
                getSupportFragmentManager().beginTransaction().replace(R.id.main_frame,select_fragment).commit();

            }

            @SuppressLint("ResourceAsColor")
            @Override
            public void onItemReselected(int itemIndex, String itemName) {
                Toast.makeText(MainActivity.this, itemIndex + " " + itemName, Toast.LENGTH_SHORT).show();
                if(change==1){
                    change=0;
                    switch (itemIndex){
                        case ID_HOME:
                            select_fragment=new chatsFragment();
                            break;
                        case ID_Group:
                            select_fragment=new chatsFragment();
                            Toast.makeText(MainActivity.this,  "Soon will make" + itemName, Toast.LENGTH_SHORT).show();
                            break;
                        case ID_PROFILE:
                            select_fragment=new ProfileFragment();
                            break;
                        case ID_LOGOUT:
                            user_logout();
                        default:
                            select_fragment=new chatsFragment();
                            break;

                    }
                    assert select_fragment != null;
                    getSupportFragmentManager().beginTransaction().replace(R.id.main_frame,select_fragment).commit();
                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(currentUser!=null){
            setuserdata();
        }
    }

    private void setuserdata() {
        String currentUserId=mAuth.getCurrentUser().getUid();
        myRef.child("Users").child(currentUserId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if((dataSnapshot.child("status").exists()))
                    Toast.makeText(MainActivity.this, dataSnapshot.child("firstName").getValue().toString(), Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void user_logout() {
        mAuth.signOut();
        Intent intent=new Intent(MainActivity.this, loginActivity.class);
        startActivity(intent);
        finish();
    }

}
